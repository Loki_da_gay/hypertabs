package tasks;

import data.Settings;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.commons.math.Random;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.input.Keyboard;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class Enter extends Task {

    private SceneObject portal;

    @Override
    public boolean validate() {
        portal = SceneObjects.getNearest("Portal");
        return Settings.hasExchanged && portal != null && Inventory.contains(1761);
    }

    @Override
    public int execute() {
        portal = SceneObjects.getNearest("Portal");

        if (portal != null) {
            //HyperTabs.status = "Entering house";
            portal.interact("Friend's house");
            Time.sleepUntil(() -> Interfaces.isVisible(162, 44), 10000);
            if (Interfaces.isVisible(162, 44)) {
                if (Interfaces.getComponent(162, 40, 0) != null) {
                    //String host = Interfaces.getComponent(162, 40, 0).getText();
                    if (Interfaces.getComponent(162, 40, 0).isVisible()) {
                        Keyboard.sendText("im really hi");
                        Time.sleep(Random.low(400, 800));
                        Keyboard.pressEnter();
                        Time.sleepUntil(() -> SceneObjects.getNearest("Lectern") != null && !Interfaces.isOpen(71), 1000, 10000);
                        Time.sleep(500, 1200);
                    }
                }
            }
        }
        return 600;
    }
}
