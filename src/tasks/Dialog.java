package tasks;

import org.rspeer.script.task.Task;

public class Dialog extends Task {
    @Override
    public boolean validate() {
        return org.rspeer.runetek.api.component.Dialog.isProcessing();
    }

    @Override
    public int execute() {
        if (org.rspeer.runetek.api.component.Dialog.isProcessing()) {
            org.rspeer.runetek.api.component.Dialog.processContinue();
        }
        return 600;
    }
}
