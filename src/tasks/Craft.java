package tasks;

import data.Settings;
import org.rspeer.runetek.adapter.scene.SceneObject;
import org.rspeer.runetek.api.commons.Time;
import org.rspeer.runetek.api.component.Interfaces;
import org.rspeer.runetek.api.component.tab.Inventory;
import org.rspeer.runetek.api.scene.SceneObjects;
import org.rspeer.script.task.Task;

public class Craft extends Task {

    private int PARENT = 79;
    private int TELEPORT = 17;
    private SceneObject lectern;

    @Override
    public boolean validate() {
        lectern = SceneObjects.getNearest("Lectern");
        return Settings.hasExchanged && lectern != null && Inventory.contains("Law rune") && Inventory.contains(1761);
    }

    @Override
    public int execute() {
        lectern = SceneObjects.getNearest("Lectern");

        if (!Interfaces.isOpen(PARENT)) {
            //HyperTabs.status = "Studying";
            lectern.interact("Study");
            Time.sleepUntil(() -> Interfaces.isOpen(PARENT), 5000);
        }
        if (Interfaces.getComponent(PARENT, TELEPORT) != null) {
            Interfaces.getComponent(PARENT, TELEPORT).interact("Make-All");
           // HyperTabs.status = "Crafting";
            Time.sleepUntil(() -> Inventory.getCount(false, "Soft clay") == 1, 60000);
            Settings.hasExchanged = false;
        }
        return 600;
    }
}
